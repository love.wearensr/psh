package main

import(
	"fmt"
	"log"
	"os"

	"gitlab.com/love.wearensr/psh/internal/pkg/config"
	"gitlab.com/love.wearensr/psh/internal/pkg/psh"
)

func main() {
	// Load psh configs
	config, err := config.LoadConfig()
	if err != nil {
		log.Fatal(err)
		os.Exit(1)
	}

	// Load the psh file
	cmd, err := psh.Load(config.PshFile)
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}

	// Try to run the specified command
	err = psh.Run(cmd, os.Args[1:])
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
}
