package config

import (
	"bufio"
	"os"
	"os/user"
	"path"
	"regexp"
)

type Config struct {
	PshFile string
}

func LoadConfig() (*Config, error) {
	// Get the current user (to get the home directory)
	usr, err := user.Current()
	if err != nil {
		return nil, err
	}

	// Get the current working directory
	wd, err := os.Getwd()
	if err != nil {
		return nil, err
	}

	// Create default config
	defaultConfig := Config{
		PshFile: path.Join(wd, ".psh.json"),
	}

	// Try loading global config from ~/.pshrc, ignoring errors because config is optional
	globalPshrcPath := path.Join(usr.HomeDir, ".pshrc")
	globalConfig, _ := parseConfig(globalPshrcPath)

	// Try loading local config from .pshrc in the current working directory
	localPshrcPath := path.Join(wd, ".pshrc")
	localConfig, _ := parseConfig(localPshrcPath)

	config := mergeConfigs(&defaultConfig, globalConfig, localConfig)

	return config, nil
}

func parseConfig(filePath string) (*Config, error) {
	// Instantiate a Config struct to hold config properties
	config := &Config{}

	// Get the current working directory
	wd, err := os.Getwd()
	if err != nil {
		return config, err
	}

	// Check file exists
	if _, err := os.Stat(filePath); os.IsNotExist(err) {
		return config, err
	}

	// Open file descriptor
	file, err := os.Open(filePath)
	if err != nil {
		return config, err
	}

	// Close fd after function returns
	defer file.Close()

	// Create regex to match properties in the config file
	propRegex, err := regexp.Compile("^(.*?)=(.*)$")
	if err != nil {
		return config, err
	}

	// Iterate lines of config file, matching properties and setting values in the config struct
	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		if m := propRegex.FindStringSubmatch(scanner.Text()); m != nil {
			switch m[1] {
			case "pshfile":
				config.PshFile = path.Join(wd, m[2])
			}
		}
	}

	if err := scanner.Err(); err != nil {
		return config, err
	}

	return config, nil
}

func mergeConfigs(configs ...*Config) *Config {
	config := &Config{}
	for _, c := range configs {
		if c.PshFile != "" {
			config.PshFile = c.PshFile
		}
	}

	return config
}
