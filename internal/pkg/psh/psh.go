package psh

import(
	"encoding/json"
	"fmt"
	"io/ioutil"
	"os/exec"
)

type Command struct {
	Name string
	Description string
	Commands []Command
	Script string
}

func Load(pshFile string) (*Command, error) {
	configStr, err := ioutil.ReadFile(pshFile)
	if err != nil {
		return nil, err
	}

	var cmd Command
	err = json.Unmarshal(configStr, &cmd)
	if err != nil {
		return nil, err
	}

	return &cmd, nil
}

func Run(c *Command, path []string) error {
	if len(path) == 0 {
		cmd := exec.Command("sh", "-c", c.Script)
		out, err := cmd.CombinedOutput()
		if err != nil {
			return err
		}
		fmt.Println(string(out))
		return nil
	}

	cStr := path[0]
	for _, sub := range c.Commands {
		if sub.Name == cStr {
			return Run(&sub, path[1:])
		}
	}

	return fmt.Errorf("Command not found: %s", cStr)
}
